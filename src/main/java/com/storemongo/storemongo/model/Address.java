package com.storemongo.storemongo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Address {
	private String nameStreet;
	private String number;
	private String reference;
	private Ubigeo ubigeo;
}
