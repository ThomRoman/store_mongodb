package com.storemongo.storemongo.model;

import com.storemongo.storemongo.dto.ClientDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document
public class Client {
	@Id
	private String id;
	private String name;
	private String lastname;
	private String paymentMethod;
	private double total;
	private List<Product> products;
	private List<Address> addresses;

	public Client(ClientDTO clientDTO){
		String[] validPaymentMethods = {"Efectivo", "Visa", "Mastercard"};

		if(!Arrays.asList(validPaymentMethods).contains(clientDTO.getPaymentMethod())){
			throw new IllegalStateException("No se admite el método de pago "+clientDTO.getPaymentMethod());
		}
		double discount = 0;
		if(clientDTO.getPaymentMethod().equals("Efectivo")){
			discount = 0.2;
		}
		if(clientDTO.getPaymentMethod().equals("Visa")){
			discount = 0.1;
		}

		this.total = 0;
		this.products = clientDTO.getProducts();
		this.addresses = clientDTO.getAddresses();

		this.name = clientDTO.getName();
		this.lastname = clientDTO.getLastname();
		this.paymentMethod = clientDTO.getPaymentMethod();

		int totalProducts = 0;

		for (var product : clientDTO.getProducts()){
			this.total+=product.getPrice()*product.getQuantity();
			totalProducts+=product.getQuantity();
		}
		if(totalProducts < 10) {
			throw new IllegalStateException("No se pueden comprar menos de 10 productos");
		}
		this.total-=this.total*discount;

	}

}
