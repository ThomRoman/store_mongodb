package com.storemongo.storemongo.dto;

import com.storemongo.storemongo.model.Address;
import com.storemongo.storemongo.model.Product;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ClientDTO {
	private String name;
	private String lastname;
	private String paymentMethod;
	private List<Address> addresses;
	private List<Product> products;
}
