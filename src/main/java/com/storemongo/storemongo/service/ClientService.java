package com.storemongo.storemongo.service;

import com.storemongo.storemongo.dto.ClientDTO;
import com.storemongo.storemongo.model.Client;

import java.util.List;

public interface ClientService {
	Client createClient(ClientDTO clientDTO);

	List<Client> getClients();
	List<Client> getClientByName(String name);
	List<Client> getClientByNameStreet(String street);
	List<Client> getClientByTotal(Double total);
}
