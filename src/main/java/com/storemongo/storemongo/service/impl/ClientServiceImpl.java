package com.storemongo.storemongo.service.impl;

import com.storemongo.storemongo.dto.ClientDTO;
import com.storemongo.storemongo.model.Client;
import com.storemongo.storemongo.repository.ClientRepository;
import com.storemongo.storemongo.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientServiceImpl implements ClientService {

	@Autowired
	private ClientRepository clientRepository;

	@Override
	public Client createClient(ClientDTO clientDTO) {
		Client client = new Client(clientDTO);
		return clientRepository.createClient(client);
	}

	@Override
	public List<Client> getClients() {
		return clientRepository.getClients();
	}

	@Override
	public List<Client> getClientByName(String name) {
		return clientRepository.getClientByName(name);
	}

	@Override
	public List<Client> getClientByNameStreet(String street) {
		return clientRepository.getClientByNameStreet(street);
	}

	@Override
	public List<Client> getClientByTotal(Double total) {
		return clientRepository.getClientByTotal(total);
	}
}
