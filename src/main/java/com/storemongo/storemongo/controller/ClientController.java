package com.storemongo.storemongo.controller;

import com.storemongo.storemongo.dto.ClientDTO;
import com.storemongo.storemongo.model.Client;
import com.storemongo.storemongo.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/v1/clients")
public class ClientController {

	@Autowired
	private ClientService clientService;

	@GetMapping
	public ResponseEntity<List<Client>> getClients(){
		var response = clientService.getClients();
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	@PostMapping
	public ResponseEntity<Object> createClient(@RequestBody ClientDTO clientDTO){
		try {
			var response = clientService.createClient(clientDTO);
			return ResponseEntity.status(HttpStatus.OK).body(response);
		}catch (Exception ex){
			Map<String,String> body = new HashMap<>();
			body.put("message_error",ex.getMessage());
			return ResponseEntity.internalServerError().body(body);
		}
	}

	@GetMapping("/name/{name}")
	public ResponseEntity<List<Client>> getClientByName(@PathVariable String name){
		var response = clientService.getClientByName(name);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	@GetMapping("/street/{street}")
	public ResponseEntity<List<Client>> getClientByNameStreet(@PathVariable String street){
		var response = clientService.getClientByNameStreet(street);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	@GetMapping("/total/{total}")
	public ResponseEntity<List<Client>> getClientByTotal(@PathVariable Double total){
		var response = clientService.getClientByTotal(total);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
}
