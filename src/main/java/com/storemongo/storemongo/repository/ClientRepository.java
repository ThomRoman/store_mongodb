package com.storemongo.storemongo.repository;

import com.storemongo.storemongo.model.Client;

import java.util.List;

public interface ClientRepository {
	Client createClient(Client client);

	List<Client> getClients();
	List<Client> getClientByName(String name);
	List<Client> getClientByNameStreet(String street);
	List<Client> getClientByTotal(Double total);
}
