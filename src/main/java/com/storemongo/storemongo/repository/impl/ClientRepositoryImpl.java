package com.storemongo.storemongo.repository.impl;

import com.storemongo.storemongo.model.Client;
import com.storemongo.storemongo.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;
import org.springframework.data.mongodb.core.query.Query;
import java.util.List;

@Repository
public class ClientRepositoryImpl implements ClientRepository {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public Client createClient(Client client) {
		return mongoTemplate.insert(client,"Client");
	}

	@Override
	public List<Client> getClients() {
		return mongoTemplate.findAll(Client.class, "Client");
	}

	@Override
	public List<Client> getClientByName(String name) {
		Query query = new Query();
		query.addCriteria(Criteria.where("name").is(name));
		return mongoTemplate.find(query, Client.class, "Client");
	}

	@Override
	public List<Client> getClientByNameStreet(String street) {
		Query query = new Query();
		query.addCriteria(Criteria.where("addresses.nameStreet").is(street));
		return mongoTemplate.find(query, Client.class, "Client");
	}

	@Override
	public List<Client> getClientByTotal(Double total) {
		Query query = new Query();
		query.addCriteria(Criteria.where("total").gt(total));
		return mongoTemplate.find(query, Client.class, "Client");

	}
}
